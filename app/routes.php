<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('login', array('uses' => 'HomeController@showLogin'));
Route::post('login', array('uses' => 'HomeController@doLogin'));
Route::get('logout', array('uses' => 'HomeController@doLogout'));


Route::group(array('before' => array('auth')), function()
{
    
    Route::get('cargaHoraria', array('uses' => 'ProfessorController@getCargaHoraria'));
    Route::get('jsonCargaHoraria', array('uses' => 'ProfessorController@jsonCargaHoraria'));
    Route::get('choqueHorario', array('uses' => 'AlocacaoHorarioController@getChoqueHorario'));
    Route::get('jsonChoqueHorario', array('uses' => 'AlocacaoHorarioController@jsonChoqueHorario'));
    
    
    Route::get('/', 'DashBoardController@index');
    
    Route::group(array('before' => array('admin')), function() {
        //Route::get('/', 'DashBoardController@index');
        
        Route::get('/disciplina', 'DisciplinaController@index');
        Route::get('/disciplina/novo', 'DisciplinaController@create');
        Route::post('/disciplina/novo', array('as' => 'disciplina.save', 'uses' => 'DisciplinaController@store'));
        Route::get('/disciplina/remover/{id}', 'DisciplinaController@destroy');
        Route::get('/disciplina/{id}/edit', 'DisciplinaController@edit');
        Route::post('/disciplina/{id}/edit', array('as' => 'disciplina.update', 'uses' => 'DisciplinaController@update'));
        
        Route::get('/area', 'AreaController@index');
        Route::get('/area/novo', 'AreaController@create');
        Route::post('/area/novo', array('as' => 'area.save', 'uses' => 'AreaController@store'));
        Route::get('/area/remover/{id}', 'AreaController@destroy');
        Route::get('/area/{id}/edit', 'AreaController@edit');
        Route::post('/area/{id}/edit', array('as' => 'area.update', 'uses' => 'AreaController@update'));
        
        Route::get('/professor', 'ProfessorController@index');
        Route::get('/professor/novo', 'ProfessorController@create');
        Route::post('/professor/novo', array('as' => 'professor.save', 'uses' => 'ProfessorController@store'));
        Route::get('/professor/remover/{id}', 'ProfessorController@destroy');
        Route::get('/professor/{id}/edit', 'ProfessorController@edit');
        Route::post('/professor/{id}/edit', array('as' => 'professor.update', 'uses' => 'ProfessorController@update'));
        
        Route::get('/curso', 'CursoController@index');
        Route::get('/curso/novo', 'CursoController@create');
        Route::post('/curso/novo', array('as' => 'curso.save', 'uses' => 'CursoController@store'));
        Route::get('/curso/remover/{id}', 'CursoController@destroy');
        Route::get('/curso/{id}/edit', 'CursoController@edit');
        Route::post('/curso/{id}/edit', array('as' => 'curso.update', 'uses' => 'CursoController@update'));
    });
    
    Route::group(array('before' => array('coordenador')), function() {
        //Route::get('/', 'DashBoardController@index');
        
        Route::get('/matriz', 'MatrizCurricularController@index');
        Route::get('/matriz/novo', 'MatrizCurricularController@create');
        Route::post('/matriz/novo/{id}', array('as' => 'matriz.save', 'uses' => 'MatrizCurricularController@store'));
        Route::get('/matriz/remover/{idCurso}/{idDisc}', 'MatrizCurricularController@destroy');
    });
     
    Route::group(array('before' => array('supervisorOrCordenador')), function() {
        //Route::get('/', 'DashBoardController@index');
        
        Route::get('/alocacaop', 'AlocacaoProfessorController@index');
        Route::get('/alocacaop/novo', 'AlocacaoProfessorController@create');
        Route::post('/alocacaop/novo/{id}', array('as' => 'alocacaop.save', 'uses' => 'AlocacaoProfessorController@store'));
        Route::get('/alocacaop/remover/{id}', 'AlocacaoProfessorController@destroy');
        
        Route::get('/alocacaoh', 'AlocacaoHorarioController@index');
        Route::get('/alocacaoh/todos', 'AlocacaoHorarioController@liste');
        Route::post('/alocacaoh/novo', 'AlocacaoHorarioController@store');
        Route::any('/alocacaoh/remover/{id}', 'AlocacaoHorarioController@destroy');
    });
});

 // ------------------------- FILTERS ---------------------------

Route::filter('authenticate', function ()
{

    /*if (!Session::has('AuthenticatedUser'))
    {
        return Redirect::to(URL::to('/login'));
    }*/
});
