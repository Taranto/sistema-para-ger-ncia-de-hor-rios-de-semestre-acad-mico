<?php

class DisciplinaController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $dsps = Disciplina::select('id','nome')->paginate(30);
        
        $this->sidemenu['menu'][1]->active = true;
        
        $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('Disciplina.index')->with('disciplinas', $dsps);
    }
    
    /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $this->sidemenu['menu'][1]->active = true;
        
        $disc['areas']  = Area::all()->lists('nome','id');
        
        $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('Disciplina.create')->with('disc', (object) $disc);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $validation= Disciplina::validate(\Input::all());
        
		if($validation->passes()){  
		    $disciplina = new Disciplina;
		    DisciplinaController::getInput($disciplina);
			$disciplina->save();
			return Redirect::to('/disciplina')->with('message', trans('Tudo jóia'));
	    }else {
			return Redirect::to('/disciplina/novo')->withInput()->withErrors($validation->errors());
		};
	}
	
	
	/**
	 * Show the form for updating a resource.
	 *
	 * @return Response
	 */
	public function edit($id)
	{
        $this->sidemenu['menu'][1]->active = true;
        
        $disc['areas']  = Area::all()->lists('nome','id');
        $disc['disciplina'] = Disciplina::find($id);
        
        $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('Disciplina.edit')->with('disc', (object) $disc);
	}
	
	/**
	 * Update the specified resource from storage.
	 *
	 * @return Response
	 */
	public function update($disciplinaId){
		$validation = Disciplina::validate(\Input::all());
		if($validation->passes()){
			$disciplina = Disciplina::find($disciplinaId);
			DisciplinaController::getInput($disciplina);
			$disciplina->save();
			return Redirect::to('/disciplina')->with('message', trans('Tudo jóia'));
		}else {
			return Redirect::to('/disciplina/novo')->withInput()->withErrors($validation->errors());
		};
	}	
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $sub = Disciplina::findOrFail($id);
        if($sub->delete()){
            return Redirect::to(URL::previous())->with('message', trans('subscriber.success_remove'));
        }else{
            return Redirect::to(URL::previous())->with('message', trans('subscriber.fail_remove'));
        }
	}
	
	public function getInput($disciplina){
	    $disciplina->nome  = \Input::get('nome');
	    $disciplina->area_id  = \Input::get('area_id');
	}
}