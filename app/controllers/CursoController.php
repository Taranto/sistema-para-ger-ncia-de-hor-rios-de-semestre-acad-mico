<?php

class CursoController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $curso = Curso::select('id','nome')->paginate(30);
        
        $this->sidemenu['menu'][2]->active = true;
        
        $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('Curso.index')->with('cursos', $curso);
    }
    
    /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    $this->sidemenu['menu'][2]->active = true;
	    
	    $curso['professores']  = Professor::docenteCoord()->lists('nome','id');
	    
		$this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('Curso.create')->with('curso', (object) $curso);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $validation= Curso::validate(\Input::all());
		if($validation->passes()){  
		    $curso = new Curso;
		    CursoController::getInput($curso);
			$curso->save();
			return Redirect::to('/curso')->with('message', trans('Tudo jóia'));
	    }else {
			return Redirect::to('/curso/novo')->withInput()->withErrors($validation->errors());
		};
	}
	
	/**
	 * Show the form for updating a resource.
	 *
	 * @return Response
	 */
	public function edit($id)
	{
        $this->sidemenu['menu'][2]->active = true;
        
        $curso['professores']  = Professor::docenteCoord()->lists('nome','id');
        $curso['curso'] = Curso::find($id);
        
        $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('Curso.edit')->with('curso', (object) $curso);
	}
	
	/**
	 * Update the specified resource from storage.
	 *
	 * @return Response
	 */
	public function update($cursoId){
		$validation = Curso::validate(\Input::all());
		if($validation->passes()){
			$curso = Curso::find($cursoId);
			CursoController::getInput($curso);
			$curso->save();
			return Redirect::to('/curso')->with('message', trans('Tudo jóia'));
		}else {
			return Redirect::to('/curso/novo')->withInput()->withErrors($validation->errors());
		};
	}	
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $sub = SubscriberModel::with('fields')->with('subscribed_lists')->findOrFail($id);
        $sub->subscribed_lists()->detach();
        $sub->fields()->delete();
        if($sub->delete()){
            return Redirect::to(URL::previous())->with('message', trans('subscriber.success_remove'));
        }else{
            return Redirect::to(URL::previous())->with('message', trans('subscriber.fail_remove'));
        }
	}
	
	public function getInput($curso){
	    $curso->nome     = \Input::get('nome');
	    $curso->professor_id  = \Input::get('professor_id');
	}
}