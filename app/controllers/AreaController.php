<?php

class AreaController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $area = Area::select('id','nome')->paginate(30);
        
        $this->sidemenu['menu'][3]->active = true;
        
        $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('Area.index')->with('areas', $area);
    }
    
    /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->sidemenu['menu'][3]->active = true;
        
        $area['professores']  = Professor::docentesup()->lists('nome','id');
        
        $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('Area.create')->with('area', (object) $area);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $validation= Area::validate(\Input::all());
		if($validation->passes()){  
		    $area = new Area;
		    AreaController::getInput($area);
			$area->save();
			return Redirect::to('/area')->with('message', trans('Tudo jóia'));
	    }else {
			return Redirect::to('/area/novo')->withInput()->withErrors($validation->errors());
		};
	}
	
	/**
	 * Show the form for updating a resource.
	 *
	 * @return Response
	 */
	public function edit($id)
	{
        $this->sidemenu['menu'][3]->active = true;
        
        $area['professores']  = Professor::docenteSup()->lists('nome','id');
        $area['area'] = Area::find($id);
        
        $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('Area.edit')->with('area', (object) $area);
	}
	
	/**
	 * Update the specified resource from storage.
	 *
	 * @return Response
	 */
	public function update($areaId){
		$validation = Area::validate(\Input::all());
		if($validation->passes()){
			$area = Area::find($areaId);
			AreaController::getInput($area);
			$area->save();
			return Redirect::to('/area')->with('message', trans('Tudo jóia'));
		}else {
			return Redirect::to('/area/novo')->withInput()->withErrors($validation->errors());
		};
	}	
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $sub = SubscriberModel::with('fields')->with('subscribed_lists')->findOrFail($id);
        $sub->subscribed_lists()->detach();
        $sub->fields()->delete();
        if($sub->delete()){
            return Redirect::to(URL::previous())->with('message', trans('subscriber.success_remove'));
        }else{
            return Redirect::to(URL::previous())->with('message', trans('subscriber.fail_remove'));
        }
	}
	
	public function getInput($area){
	    $area->nome  = \Input::get('nome');
	    $area->professor_id  = \Input::get('professor_id');
	}
}