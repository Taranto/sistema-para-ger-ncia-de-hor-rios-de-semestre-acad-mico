<?php

class BaseController extends Controller {
    protected $layout = 'base';
    protected $sidemenu;

    function __construct()
    {
        if (Auth::user()->tipo == "adm"){
            $this->sidemenu = array(
                'user' => Auth::user()->nome,
                'menu' => array(
                    
                    (object) array( 'active' => false, 'url' => URL::to('/'), 'name' => 'Dashboard', 'icon' => 'fa-dashboard', 'submenu' => null),
                    (object) array( 'active' => false, 'url' => URL::to('/disciplina'), 'name' => 'Cadastro de Disciplinas', 'icon' => 'fa-book', 'submenu' => null), 
                    (object) array( 'active' => false, 'url' => URL::to('/curso'), 'name' => 'Cadastro de Cursos', 'icon' => 'fa-graduation-cap', 'submenu' => null),
                    (object) array( 'active' => false, 'url' => URL::to('/area'), 'name' => 'Cadastro de Areas', 'icon' => 'fa-archive', 'submenu' => null),
                    (object) array( 'active' => false, 'url' => URL::to('/professor'), 'name' => 'Cadastro de Professores', 'icon' => 'fa-user', 'submenu' => null),
                ),
            );
        } elseif (Auth::user()->tipo == "coord")
        {
            $this->sidemenu = array(
                'user' => Auth::user()->nome,
                'menu' => array(
                    
                    (object) array( 'active' => false, 'url' => URL::to('/'), 'name' => 'Dashboard', 'icon' => 'fa-dashboard', 'submenu' => null),
                    (object) array( 'active' => false, 'url' => URL::to('/alocacaoh'), 'name' => 'Definição de Horário', 'icon' => 'fa-graduation-cap', 'submenu' => null),
                    (object) array( 'active' => false, 'url' => URL::to('/alocacaop'), 'name' => 'Professor x Matéria', 'icon' => 'fa-archive', 'submenu' => null),
                    (object) array( 'active' => false, 'url' => URL::to('/cargaHoraria'), 'name' => 'Carga Horária', 'icon' => 'fa-archive', 'submenu' => null),
                    (object) array( 'active' => false, 'url' => URL::to('/choqueHorario'), 'name' => 'Choque de Horário', 'icon' => 'fa-archive', 'submenu' => null),
                    (object) array( 'active' => false, 'url' => URL::to('/matriz'), 'name' => 'Matriz Curricular', 'icon' => 'fa-book', 'submenu' => null), 


                ),
            );
        } else
        {
            $this->sidemenu = array(
                'user' => Auth::user()->nome,
                'menu' => array(
                    
                    (object) array( 'active' => false, 'url' => URL::to('/'), 'name' => 'Dashboard', 'icon' => 'fa-dashboard', 'submenu' => null),
                    (object) array( 'active' => false, 'url' => URL::to('/alocacaoh'), 'name' => 'Definição de Horário', 'icon' => 'fa-graduation-cap', 'submenu' => null),
                    (object) array( 'active' => false, 'url' => URL::to('/alocacaop'), 'name' => 'Professor x Matéria', 'icon' => 'fa-archive', 'submenu' => null),
                    (object) array( 'active' => false, 'url' => URL::to('/cargaHoraria'), 'name' => 'Carga Horária', 'icon' => 'fa-archive', 'submenu' => null),
                    (object) array( 'active' => false, 'url' => URL::to('/choqueHorario'), 'name' => 'Choque de Horário', 'icon' => 'fa-archive', 'submenu' => null),
                    
                ),
            );
        }
    }

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
