<?php

class Subscriber extends \BaseController {

    function __construct()
    {
        $this->mainmenu = array(makeButton(array(URL::to('/subscriber'),trans('base.subscribers'),true)),
            makeButton(array(URL::to('/list'),trans('base.lists'))),
            makeButton(array(URL::to('/template'),trans('base.templates'))),
            makeButton(array(URL::to('/campaign'),trans('base.campaigns'))),
            makeButton(array(URL::to('/settings'),trans('base.settings'))),
        );
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $this->layout->header = View::make('header')->with('header',array('title'=>'Versenden', 'menu' => $this->mainmenu));

        $leftmenu = array(makeButton(array(URL::to('/subscriber/new'),trans('subscriber.create_new'))),
                          makeButton(array(URL::to('/subscriber/export'),trans('subscriber.export'))),);

        $subs = (!Input::has('search')) ? SubscriberModel::select('id','name')->paginate(30) : SubscriberModel::where('name', 'like', '%'.Input::get('search').'%')->paginate(30) ;

        $Subscriber = (object)array('subs'=> $subs );
        $this->layout->sidebar = View::make('sidebar')->with('sidebar',array('menu' => $leftmenu));
        $this->layout->content = View::make('Subscriber.index')->with('Subscriber',$Subscriber);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $this->layout->header = View::make('header')->with('header',array('title'=>'Versenden', 'menu' => $this->mainmenu));

        $leftmenu = array(makeButton(array(URL::to('subscriber/new'),trans('subscriber.email'))));

        $this->layout->sidebar = View::make('sidebar')->with('sidebar',array('menu' => $leftmenu));

        $this->layout->content = View::make('Subscriber.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $sub  = SubscriberModel::onlyTrashed()->where('email', Input::get('email'))->first();

        if(!is_null($sub))
            $sub->restore();
        else
            $sub = new SubscriberModel();

        if($sub->save()){
            return Redirect::to('/subscriber/edit/'.$sub->id)->with('message', trans('subscriber.success_create'));
        }else{
            return Redirect::to('/subscriber/new')->withInput()->withErrors($sub->errors());
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
     * @todo: SEND ACTIVATION EMAIL option
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $this->layout->header = View::make('header')->with('header',array('title'=>'Versenden', 'menu' => $this->mainmenu));

        $leftmenu = array(makeButton(array(URL::to("/subscriber/edit/$id"),trans('subscriber.email'))),
            makeButton(array(URL::to("/subscriber/edit/$id/lists"),trans('subscriber.lists'))),
            makeButton(array(URL::to("subscriber/edit/$id/fields"),trans('subscriber.fields'))),);

        $this->layout->sidebar = View::make('sidebar')->with('sidebar',array('menu' => $leftmenu));

        $sub  = SubscriberModel::findOrFail($id);
        $this->layout->content = View::make('Subscriber.edit')->with('sub',$sub);
	}

    /**
     * Show the form for editing the subscriptions.
     *
     * @param  int  $id
     * @return Response
     */
    public function editLists($id)
    {
        $this->layout->header = View::make('header')->with('header',array('title'=>'Versenden', 'menu' => $this->mainmenu));

        $leftmenu = array(makeButton(array(URL::to("/subscriber/edit/$id"),trans('subscriber.email'))),
            makeButton(array(URL::to("/subscriber/edit/$id/lists"),trans('subscriber.lists'))),
            makeButton(array(URL::to("subscriber/edit/$id/fields"),trans('subscriber.fields'))),);

        $this->layout->sidebar = View::make('sidebar')->with('sidebar',array('menu' => $leftmenu));

        $sub['model']  = SubscriberModel::findOrFail($id);
        $sub['lists']  = ListModel::all()->lists('name','id');
        $sub['subscribed_lists'] = $sub['model']->subscribed_lists()->select('id','name')->paginate(30);
        $this->layout->content = View::make('Subscriber.lists')->with('sub',(object)$sub);
    }

    /**
     * Show the form for editing the subscriptions.
     *
     * @param  int  $id
     * @return Response
     */
    public function displayFields($id)
    {
        $this->layout->header = View::make('header')->with('header',array('title'=>'Versenden', 'menu' => $this->mainmenu));

        $leftmenu = array(makeButton(array(URL::to("/subscriber/edit/$id"),trans('subscriber.email'))),
            makeButton(array(URL::to("/subscriber/edit/$id/lists"),trans('subscriber.lists'))),
            makeButton(array(URL::to("subscriber/edit/$id/fields"),trans('subscriber.fields'))),);

        $this->layout->sidebar = View::make('sidebar')->with('sidebar',array('menu' => $leftmenu));

        $sub['lists']  = SubscriberModel::findOrFail($id)->subscribed_lists()->get();
        $sub['id']  = $id;

        $this->layout->content = View::make('Subscriber.fields')->with('sub',(object)$sub);
    }

    /**
     * Show the form for editing the subscriptions.
     *
     * @param  int  $id
     * @param  int  $list_id
     * @return Response
     */
    public function editFields($id,$list_id)
    {
        $this->layout->header = View::make('header')->with('header',array('title'=>'Versenden', 'menu' => $this->mainmenu));

        $leftmenu = array(makeButton(array(URL::to("/subscriber/edit/$id"),trans('subscriber.email'))),
            makeButton(array(URL::to("/subscriber/edit/$id/lists"),trans('subscriber.lists'))),
            makeButton(array(URL::to("subscriber/edit/$id/fields"),trans('subscriber.fields'))),);

        $this->layout->sidebar = View::make('sidebar')->with('sidebar',array('menu' => $leftmenu));

        $list = ListModel::with('fieldtype', 'fieldtype.field')->findOrFail($list_id);
        $sub['field_types']  = $list->fieldtype()->get();
        $sub['id']  = $id;
        $sub['list']  = $list;

        $this->layout->content = View::make('Subscriber.fields_edit')->with('sub',(object)$sub);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param  int  $list_id
     * @return Response
     */
    public function updateField($id, $list_id)
    {
        $fields = Input::get('field');
        $success = true;
        foreach($fields as $field){
            if(!isset($field['id'])){
                $f = new Field();
                $f->field_type_id = $field['field_type_id'];
                $f->subscriber_id = $id;
            }else{
                $f = Field::find($field['id']);
            }

            $f->value = $field['value'];

            if(!$f->save()) $success = false;
        }

        if($success){
            return Redirect::to(URL::previous())->with('message', trans('subscriber.success_field_update'));
        }else{
            return Redirect::to(URL::previous())->with('message', trans('subscriber.fail_field_update'));
        }
    }


    /**
     * Remove list subscription.
     *
     * @param  int  $id
     * @return Response
     */
    public function unsubscribeFromList($id)
    {
        $list_id = Input::get('list_id');
        $sub = SubscriberModel::findOrFail($id);
        $list = $sub->subscribed_lists()->findOrFail($list_id);
        if($sub->subscribed_lists()->detach($list->id)){
            return Redirect::to(URL::previous())->with('message', trans('subscriber.success_unsubscribe'));
        }else{
            return Redirect::to(URL::previous())->with('message', trans('subscriber.fail_unsubscribe'));
        }
    }

    /**
     * Subscribre to list.
     *
     * @param int $id
     * @return Response
     */
    public function subscribeToList($id)
    {

        $list_id = Input::get('list_id');
        $sub = SubscriberModel::with('subscribed_lists')->findOrFail($id);

        $already_subbed = $sub->subscribed_lists()->find($list_id);

        if(!is_null($already_subbed))
            return Redirect::to(URL::previous())->with('message', trans('subscriber.already_subscribe'));

        $list = ListModel::findOrFail($list_id);
        if($sub->subscribed_lists()->attach($list)){
            return Redirect::to("/subscriber/edit/$sub->id/lists")->with('message', trans('subscriber.success_subscribe'));
        }else{
            return Redirect::to("/subscriber/edit/$sub->id/lists")->with('message', trans('subscriber.fail_subscribe'));
        }
    }


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $sub = SubscriberModel::findOrFail($id);
        /*$up = function($sub){
            $sub->active = Input::has('active') ? 1 : 0;
            //dd($sub);
        };
        $sub->updating($up);*/

        if($sub->updateUniques()){
            return Redirect::to('/subscriber/edit/'.$sub->id)->with('message', trans('subscriber.success_update'));
        }else{
            return Redirect::to('/subscriber/edit/'.$sub->id)->with('message', trans('subscriber.fail_update'))->with('errors', $sub->errors());
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $sub = SubscriberModel::with('fields')->with('subscribed_lists')->findOrFail($id);
        $sub->subscribed_lists()->detach();
        $sub->fields()->delete();
        if($sub->delete()){
            return Redirect::to(URL::previous())->with('message', trans('subscriber.success_remove'));
        }else{
            return Redirect::to(URL::previous())->with('message', trans('subscriber.fail_remove'));
        }
	}

    /**
     * Send activation email to subscriber
     *
     * @param  int  $id
     * @return Response
     */
    public function emailActivation($id)
    {
        $sub = SubscriberModel::findOrFail($id);

        if($sub->save()){
            return Redirect::to('/subscriber/edit/'.$sub->id)->with('message', trans('subscriber.success_activate_email'));
        }else{
            return Redirect::to('/subscriber/edit/'.$sub->id)->with('message', trans('subscriber.fail_activate_email'));
        }
    }

    /**
     * Export to csv list of {name, email}
     *
     * @todo: create selective field export options
     * @return Response
     */
    public function export()
    {
        $subs = SubscriberModel::all();

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"subscribers.csv\";" );
        header("Content-Transfer-Encoding: binary");

        $ids = array_map(function($item) { return $item['name'].','.$item['email'].','.(is_null($item['deleted_at']) ? $item['active'] : 0); }, $subs->toArray());
        $output = implode('\n', $ids);

        echo "Name,Email,Active\n";
        die($output);
    }

}