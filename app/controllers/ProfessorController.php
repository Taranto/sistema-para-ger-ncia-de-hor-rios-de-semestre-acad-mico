<?php

class ProfessorController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $prfs = Professor::select('id','nome','tipo')->paginate(30);
        
        $this->sidemenu['menu'][4]->active = true;
        
        $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('Professor.index')->with('professores', $prfs);
    }
    
    /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    $this->sidemenu['menu'][4]->active = true;
	    
	    $prof['tipos'] = array( 'prof' => 'Professor', 'coord'  => 'Coordenador','sup'  => 'Supervisor','adm'  => 'Administrador' );
	    
		$this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('Professor.create')->with('prof', (object) $prof);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	    $input = Input::all();
	    unset($input['_token']);
        $validation= Professor::validate(\Input::all());
		if($validation->passes()){  
		    $professor = new Professor($input);
		    $professor->password = Hash::make('teste');
		    //ProfessorController::getInput($professor);
			$professor->save();
			return Redirect::to('/professor')->with('message', trans('Tudo jóia'));
	    }else {
			return Redirect::to('/professor/novo')->withInput()->withErrors($validation->errors());
		};
	}
	
	public function edit($id)
	{
        $this->sidemenu['menu'][4]->active = true;
        
        $professor['tipos'] = array( 'prof' => 'Professor', 'coord'  => 'Coordenador','sup'  => 'Supervisor','adm'  => 'Administrador' );
        $professor['professor'] = Professor::find($id);
        
        $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('Professor.edit')->with('professor', (object) $professor);
	}
	
	/**
	 * Update the specified resource from storage.
	 *
	 * @return Response
	 */
	public function update($professorId){
		$validation = Professor::validate(\Input::all());
		if($validation->passes()){
			$professor = Professor::find($professorId);
			ProfessorController::getInput($professor);
			$professor->save();
			return Redirect::to('/professor')->with('message', trans('Tudo jóia'));
		}else {
			return Redirect::to('/professor/novo')->withInput()->withErrors($validation->errors());
		};
	}	
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $sub = Professor::findOrFail($id);
        
        if($sub->delete()){
            return Redirect::to(URL::previous())->with('message', trans('subscriber.success_remove'));
        }else{
            return Redirect::to(URL::previous())->with('message', trans('subscriber.fail_remove'));
        }
	}
	
	public function getInput($professor){
	    $professor->nome  = \Input::get('nome');
	    $professor->tipo  = \Input::get('tipo');
	}
	
	public function jsonCargaHoraria(){
	    $professor = Professor::all();
	    $i = 0;
	    $carga[0] = array('nome' => '', 'cargaHoraria' => '');
	    foreach ($professor as $val) {
	        if (AlocacaoHorario::all()->first() != null){
	            foreach (AlocacaoHorario::all() as $aclocacaoHorario) {
    	            $horas = $aclocacaoHorario->alocacaoProfessor->where('professor_id', '=', $val->id)->count();
    	        }
    	        $carga[$i] = array('nome' => $val->nome, 'cargaHoraria' => $horas);
    	        $i++;
	        }
	    }
	    return $carga;
	}
	
	
	public function getCargaHoraria() {
	    $carga = self::jsonCargaHoraria();
	    
	    $this->sidemenu['menu'][3]->active = true;
	    
	    $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('Professor.carga_horarria')->with('cargas', (object) $carga);
	}
}