<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class MatrizCurricularController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try
        {
            $curso = Curso::where('professor_id', '=', Auth::user()->id)->firstOrFail();
            $disciplina = $curso->disciplinas()->get();
            $disciplinas = array();
            foreach ($disciplina as $d) {
                $disciplinas[$d->pivot->semestre][] = $d;
            }
        
            $matriz['disciplinas'] = $disciplinas;
            $matriz['curso_id'] = $curso->id;
        }
        catch(ModelNotFoundException $e)
        {
            return Redirect::to('/')->with('message', 'Você não tem nenhum Curso');
        }
        
        $this->sidemenu['menu'][5]->active = true;
        
        $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('MatrizCurricular.index')->with('matriz', (object) $matriz);
    }
    
    /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    $this->sidemenu['menu'][2]->active = true;
	     try
        {
            $curso = Curso::where('professor_id', '=', Auth::user()->id)->firstOrFail();
        
            $matriz['disciplinas'] = Disciplina::all()->lists('nome', 'id');
            $matriz['curso_id'] = $curso->id;
        }
        catch(ModelNotFoundException $e)
        {
            return Redirect::to('/')->with('message', 'Você não tem nenhum Curso');
        }
	    
		$this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('MatrizCurricular.create')->with('matriz', (object) $matriz);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{
        $dics = Input::get('disciplina_ids');
        foreach ($dics as $d) {
            $flash = new MatrizCurricular;
            $flash->curso_id = $id;
            $flash->disciplina_id = $d;
            $flash->semestre = Input::get('semestre');
			$flash->save();
        }
        
		return Redirect::to(URL::previous())->with('message', trans('Tudo jóia'));
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $idCurso
	 * @param  int  $idDisc
	 * @return Response
	 */
	public function destroy($idCurso, $idDisc)
	{
        $mat = MatrizCurricular::where('curso_id', '=', $idCurso)->where('disciplina_id', '=', $idDisc);
        
        if($mat->delete()){
            return Redirect::to(URL::previous())->with('message', trans('subscriber.success_remove'));
        }else{
            return Redirect::to(URL::previous())->with('message', trans('subscriber.fail_remove'));
        }
	}
	
	public function getInput($curso){
	    $curso->nome     = \Input::get('nome');
	    $curso->professor_id  = \Input::get('professor_id');
	}
}