<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class AlocacaoHorarioController extends \BaseController
{
    /**
     * Acesso usando de exemplo um Coordenador
     *
     * @return Response
     */
    public function index()
    {
        try
        {
            $curso = Curso::where('professor_id', '=', Auth::user()->id)->firstOrFail();
            $alocacaoH = AlocacaoHorario::lists('alocacao_professor_id');
            $alocacaoP = AlocacaoProfessor::where('curso_id', '=', $curso->id);
            $alocacaoP = sizeof($alocacaoH) > 0 ? $alocacaoP->whereNotIn('id', $alocacaoH)->get() : $alocacaoP->get();
            $alocacaoPAlocado = AlocacaoProfessor::where('curso_id', '=', $curso->id)->with('alocacaoHorario');
            $alocacaoPAlocado = sizeof($alocacaoH) > 0 ? $alocacaoPAlocado->whereIn('id', $alocacaoH)->get() : [];
        }
        catch(ModelNotFoundException $e)
        {
            return Redirect::to('/')->with('message', 'Você não tem nenhum Curso');
        }
        $this->sidemenu['menu'][1]->active = true;
        
        $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('AlocacaoHorario.index')->with('data', (object)['alocado'=>$alocacaoPAlocado, 'naoalocado'=>$alocacaoP]);
    }
    
    /**
     * Acesso usando de exemplo um Coordenador
     *
     * @return Response
     */
    public function liste()
    {
        
        $curso = Curso::where('professor_id', '=', Auth::user()->id);
        $alocacaoP = AlocacaoProfessor::where('curso_id', '=', $curso->id)->get();
        
        foreach ($alocacaoP as $a) {
            $alocacaoH =  AlocacaoHorario::where('alocacao_professor_id', '=', $a->id)->get();
        }
        return Response::json($alocacaoH);
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $data = Input::json()->all();
        $flash = new AlocacaoHorario;
        $flash->alocacao_professor_id = $data['alocacao_professor_id'];
        $flash->dia = $data['dia'];
        $flash->turno = $data['turno'];
        
        $su = $flash->save();
        self::emailChoqueHorario($flash);
		return Response::json(array('success' => $su, 'id'=>$flash->id));
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $idCurso
	 * @param  int  $idDisc
	 * @return Response
	 */
	public function destroy($id)
	{
        $mat = AlocacaoHorario::findOrFail($id);
        return Response::json(array('success' => $mat->delete()));
	}
	
	public function emailChoqueHorario($novo) 
	{
	    foreach (AlocacaoHorario::all() as $velho) {
            $EITACARALHO = true;
            if (($velho != $novo) && (($velho->dia == $novo->dia) && ($velho->turno == $novo->turno)))
            {
                if($velho->alocacaoProfessor->professor_id == $novo->alocacaoProfessor->professor_id)
                {
                    Mail::send('email', array('msg' => 'DEU MERDA, PROFESSOR SE MULTIPLICOU'), function($message)
                    {
                        $message->to('rafael.taranto5@gmail.com', 'Rafael Taranto')->subject('Choque de Horário!!!!!');
                    });
                } else
                {
                    try{
                        $curso2 = CursoDisciplina::where('curso_id', '=', $novo->alocacaoProfessor->curso->id, 'and', 'disciplina_id', '=', $novo->alocacaoProfessor->disciplina->id)->firstOrFail();
                        $curso1 = CursoDisciplina::where('curso_id', '=', $velho->alocacaoProfessor->curso->id, 'and', 'disciplina_id', '=', $velho->alocacaoProfessor->disciplina->id)->firstOrFail();
                    } catch(ModelNotFoundException $e){
                        $EITACARALHO = false;
                    }
                    if ($EITACARALHO)
                    {
                        if (($novo->alocacaoProfessor->curso->id == $velho->alocacaoProfessor->curso->id) && ($curso2->semestre == $curso1->semestre))
                        {
                            Mail::send('email', array('msg' => 'DEU MERDA, ALUNOS SE MULTIPLICARAM'), function($message)
                            {
                                $message->to('rafael.taranto5@gmail.com', 'Rafael Taranto')->subject('Choque de Horário!!!!!');
                            });
                        }
                    }    
                }
            }
	    }
	}
	
	public function jsonChoqueHorario()
	{
	    $alocacaoh = AlocacaoHorario::all();
	    $i = 0;
	    $lista[0] = 'vazio';
	    if (AlocacaoHorario::all()->first() != null)
	    {
    	    foreach ($alocacaoh as $aloc1) {
    	        foreach ($alocacaoh as $aloc2) {
    	            //BLANK IF, QUERO TUDO COM NOME DE CursoDisciplina
    	            if($aloc1->alocacaoProfessor->curso->id == $aloc2->alocacaoProfessor->curso->id)
    	            {}
    	            $EITACARALHO = true;
    	            if (($aloc1 != $aloc2) && (($aloc1->dia == $aloc2->dia) && ($aloc1->turno == $aloc2->turno)))
    	            {
                        if($aloc1->alocacaoProfessor->professor_id == $aloc2->alocacaoProfessor->professor_id)
                        {
                            if (!in_array(array('aloc1' => $aloc2, 'aloc2' => $aloc1, 'motivo' => 'Mesmo professor em 2 turmas com mesmo horario'), $lista)) 
                            { 
                                $lista[$i] = array('aloc1' => $aloc1, 'aloc2' => $aloc2, 'motivo' => 'Mesmo professor em 2 turmas com mesmo horario');
                                $i++;
                            }
                        } else
                        {
                            try{
                                $curso2 = CursoDisciplina::where('curso_id', '=', $aloc2->alocacaoProfessor->curso_id, 'and', 'disciplina_id', '=', $aloc2->alocacaoProfessor->disciplina_id)->firstOrFail();
                                $curso1 = CursoDisciplina::where('curso_id', '=', $aloc1->alocacaoProfessor->curso_id, 'and', 'disciplina_id', '=', $aloc1->alocacaoProfessor->disciplina_id)->firstOrFail();
                            } catch(ModelNotFoundException $e){
                                $EITACARALHO = false;
    	                    }
    	                    if ($EITACARALHO)
    	                    {
    	                        if (($aloc2->alocacaoProfessor->curso_id == $aloc1->alocacaoProfessor->curso_id) && ($curso2->semestre == $curso1->semestre))
    	                        {
    	                            if (!in_array(array('aloc1' => $aloc2, 'aloc2' => $aloc1, 'motivo' => 'Mesmo Semestre com aula no mesmo horario'), $lista)) 
    	                            { 
    	                                $lista[$i] = array('aloc1' => $aloc1, 'aloc2' => $aloc2, 'motivo' => 'Mesmo Semestre com aula no mesmo horario');
    	                                $i++;
    	                            }
    	                        }    
    	                    }
    	                }
    	            }
    	        }
    	    }
	    }else 
	    {
	        $lista[0] = 'vazio';
	        return $lista;
	    }
	    //var_dump(json_encode((object)$lista));
	    return $lista;
	}
	
	public function getChoqueHorario() 
	{
	    $lista = self::jsonChoqueHorario();
	    
	    $this->sidemenu['menu'][4]->active = true;
	    //if ($lista[0] == 'vazio')
	    //{
	     //   $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
          //  $this->layout->content = View::make('AlocacaoHorario.choqueVazio');
	    //} else 
	    //{
            $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
            $this->layout->content = View::make('AlocacaoHorario.choque')->with('lista', (object)$lista);
	    //}
	    
	}
}