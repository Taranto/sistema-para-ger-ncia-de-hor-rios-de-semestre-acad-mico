<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class AlocacaoProfessorController extends \BaseController
{
    /**
     * Acesso usando de exemplo um Coordenador
     *
     * @return Response
     */
    public function index()
    {
        $alocacaoP = array();
        if (Auth::user()->tipo == 'coord')
        {
    		try
            {
                $curso = Curso::where('professor_id', '=', Auth::user()->id)->firstOrFail();
                $alocacaoP = AlocacaoProfessor::where('curso_id', '=', $curso->id)->paginate(30);
            } 
            catch(ModelNotFoundException $e)
            {
                return Redirect::to('/')->with('message', 'Você não tem nenhum Curso');
            }
        } else
        {   
            $area = Area::where('professor_id', '=', Auth::id())->get();
            try
            {
                $area = Area::where('professor_id', '=', Auth::user()->id)->firstOrFail();
                $disciplina = Disciplina::where('area_id' , '=', $area->id)->get();
                foreach ($disciplina as $disc) {
                    $lista = AlocacaoProfessor::where('disciplina_id', '=', $disc->id)->get();
                    foreach ($lista as $l) {
                        array_push($alocacaoP, $l);
                    }
                }
                $alocacaoP = (object)$alocacaoP;
            } 
            catch(ModelNotFoundException $e)
            {
                return Redirect::to('/')->with('message', 'Você não tem nenhum Curso');
            }
        }
        $this->sidemenu['menu'][2]->active = true;
        
        $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('AlocacaoProfessor.index')->with('alocacaoP', $alocacaoP);

    }
    
    
    /**
     * Acesso usando de exemplo um Supervisor
     *  
     * @return Response
     */
    public function index2()
    {
        
    }
    
    /**
	 * Create no caso de Coordenador
	 *
	 * @return Response
	 */
	public function create()
	{
	    $this->sidemenu['menu'][2]->active = true;
	    
	    $matriz['professores']  = Professor::all()->lists('nome','id');
	    $matriz['disciplinas']  = Disciplina::all()->lists('nome', 'id');
	    
	    
	    try
        {
            $curso = Curso::where('professor_id', '=', Auth::user()->id)->firstOrFail();
            $matriz['curso_id'] = $curso->id;
        } 
        catch(ModelNotFoundException $e)
        {
            return Redirect::to('/')->with('message', 'Você não tem nenhum Curso');
        }
	    
		$this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('AlocacaoProfessor.create')->with('matriz', (object) $matriz);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{
        $dics = Input::get('disciplina_ids');
        foreach ($dics as $d) {
            $flash = new AlocacaoProfessor;
            $flash->curso_id = $id;
            $flash->disciplina_id = $d;
            $flash->professor_id = Input::get('professor');
			$flash->save();
        }
        
		return Redirect::to(URL::previous())->with('message', trans('Tudo jóia'));
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $idCurso
	 * @param  int  $idDisc
	 * @return Response
	 */
	public function destroy($id)
	{
        $mat = AlocacaoProfessor::find($id);
        
        if($mat->delete()){
            return Redirect::to(URL::previous())->with('message', trans('subscriber.success_remove'));
        }else{
            return Redirect::to(URL::previous())->with('message', trans('subscriber.fail_remove'));
        }
	}
}