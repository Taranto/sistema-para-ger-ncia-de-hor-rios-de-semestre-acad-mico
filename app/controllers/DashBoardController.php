<?php

class DashBoardController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->sidemenu['menu'][0]->active = true;
        $this->layout->sidebar = View::make('sidebar')->with('data', $this->sidemenu);
        $this->layout->content = View::make('DashBoard.index');
    }
    
    public function login()
    {
        View::make('DashBoard.index');
    }

}