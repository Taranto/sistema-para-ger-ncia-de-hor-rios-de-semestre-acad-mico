<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;

class Professor extends \BaseModel implements UserInterface {
	
	use UserTrait;
	
	protected $table='professor';
	protected $fillable = array('nome', 'tipo', 'email');
	public $timestamps = false;
	protected $hidden = array('password');


	public static $rules = array(
		'nome'               => 'required|max:45',
		'tipo'               => 'required',
		'email'              => 'required|email'
		
	);	
	
    public function area(){
    	return $this->hasOne('Area');
    }
    
    public function curso(){
    	return $this->hasOne('Curso');
    }
    
    public function scopeDocenteSup($query) {
        return $query->whereRaw( 'tipo = "sup"');
    }
    
    public function scopeDocenteCoord($query) {
        return $query->whereRaw('tipo = "coord"');
    }
    
    public function alocacao(){
	    return $this->hosMany('Alocacao');
	}

}