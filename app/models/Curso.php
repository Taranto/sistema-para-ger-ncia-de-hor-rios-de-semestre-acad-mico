<?php

class Curso extends \BaseModel {
	
	protected $table='curso';
	public $timestamps = false;

	public static $rules = array(
		'nome'              => 'required|max:45',
		'area_id'			=> 'integer',
		'professor_id'      => 'unique:curso,professor_id|unique:area,professor_id'
	);	
	
	public function professor(){
	    return $this->belongsTo('Professor');
	}
	
	public function disciplinas(){
        return $this->belongsToMany('Disciplina', 'curso_disciplina')->withPivot('semestre');
    }
    
    public function alocacaoProfessor(){
	    return $this->hasMany('AlocacaoProfessor');
	}
	
	public function cursoDisciplina(){
	    return $this->hasMany('CursoDisciplina');
	}


}