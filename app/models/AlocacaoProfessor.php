<?php

class AlocacaoProfessor extends \BaseModel {

	protected $table='alocacao_professor';
	protected $fillable = array('curso_id', 'disciplina_id', 'professor_id');
	public $timestamps = false;

	public static $rules = array(
		'disciplina_id'     => 'required',
		'professor_id'      => 'required',
		'curso_id'          => 'required'
	);
	
	public function professor(){
	    return $this->belongsTo('Professor');
	}
	
	public function disciplina(){
	    return $this->belongsTo('Disciplina');
	}
	
	public function curso(){
	    return $this->belongsTo('Curso');
	}
	
	public function alocacaoHorario(){
	    return $this->hasOne('AlocacaoHorario');
	}
	

}