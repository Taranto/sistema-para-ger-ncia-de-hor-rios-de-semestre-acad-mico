<?php

class AlocacaoConflito extends \BaseModel {

	protected $table='alocacao_conflito';
	protected $fillable = array('alocaco_horario_id1', 'alocacao_horario_id2');
	public $timestamps = false;

	public static $rules = array(
		'alocaco_horario_id1'     => 'required',
		'alocacao_horario_id2'    => 'required'
	);
	
}