<?php

class Area extends \BaseModel {

	protected $table='area';
	protected $fillable = array('nome', 'professor_id');
	public $timestamps = false;

	public static $rules = array(
		'nome'               => 'required|max:45',
		'professor_id'       => 'unique:area'
	);

	
	public function disciplina(){
		return $this->hasMany('Disciplina');
	}
	
	public function professor(){
	    return $this->belongsTo('Professor');
	}
	

}