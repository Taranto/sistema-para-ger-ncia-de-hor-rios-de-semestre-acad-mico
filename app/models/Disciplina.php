<?php

class Disciplina extends \BaseModel {

	protected $table='disciplina';
	public $timestamps = false;

	public static $rules = array(
		'nome'              => 'required|max:45'
	);
	
	public function area(){
	    return $this->belongsTo('Area');
	}
	
	public function curso(){
	    return $this->belongsToMany('curso');
	}
	
	public function alocacao(){
	    return $this->hasMany('Alocacao');
	}
	
	public function cursoDisciplina(){
	    return $this->hasMany('CursoDisciplina');
	}

}