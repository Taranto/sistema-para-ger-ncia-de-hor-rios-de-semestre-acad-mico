<?php

class AlocacaoHorario extends \BaseModel {

	protected $table='alocacao_horario';
	protected $fillable = array('alocacao_professor_id', 'dia', 'turno');
	public $timestamps = false;

	public static $rules = array(
		'alocacao_professor_id'     => 'required',
		'turno'                     => 'required',
		'dia'                       => 'required'
	);
	
	public function alocacaoProfessor(){
	    return $this->belongsTo('AlocacaoProfessor');
	}

}