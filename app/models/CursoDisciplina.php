<?php

class CursoDisciplina extends \Eloquent {
	
	protected $table='curso_disciplina';
	public $timestamps = false;
	
	
	public function disciplina()
    {
        return $this->belongsTo('Disciplina');
    }

    public function curso()
    {
        return $this->belongsTo('Curso');
    }
}