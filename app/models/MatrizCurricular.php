<?php

Class MatrizCurricular extends \BaseModel
{

    protected $table='curso_disciplina';
    protected $guarded = array();
    public $timestamps = false;
    
    public static $rules = array(
		'curso_id'              => 'required',
		'disciplina_id'         => 'required',
		'semestre'              => 'required'
	);
    
}