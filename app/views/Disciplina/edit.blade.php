@section('content')

<div class="col-lg-12">
    <div class="form-panel">
        <h4 class="mb"><i class="fa fa-angle-right"></i> Editando a Disciplina {{$disc->disciplina->nome}}</h4>
          
        {{ Form::open(array('route' => array('disciplina.update', $disc->disciplina->id), 'class' => 'form-horizontal style-form')) }}
            <div class="form-group">
                {{ Form::label('area_id', 'Area', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::select('area_id', $disc->areas, $disc->disciplina->area_id, array('class'=>'form-control')) }}
                    {{$errors->first('area_id')}}
                </div>
            </div>
            
            <div class="form-group">
                {{ Form::label('nome', 'Nome', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::text('nome', $disc->disciplina->nome, array('class' => 'form-control')) }}
                    {{$errors->first('nome')}}
                </div>
            </div>
            <p>
                {{ Form::submit("Modificar Disciplina", array('class' => 'btn btn-success')) }}
            </p>
        {{ Form::close() }}
    </div>
  </div>

@stop