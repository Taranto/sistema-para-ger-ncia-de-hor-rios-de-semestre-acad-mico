@section('content')

<div class="col-lg-12">
    <div class="form-panel">
        <h4 class="mb"><i class="fa fa-angle-right"></i> Nova Disciplina </h4>
          
        {{ Form::open(array('route' => 'disciplina.save', 'class' => 'form-horizontal style-form')) }}
            <div class="form-group">
                {{ Form::label('area_id', 'Area', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::select('area_id', $disc->areas, null, array('class'=>'form-control')) }}
                    {{$errors->first('area_id')}}
                </div>
            </div>
            
            <div class="form-group">
                {{ Form::label('nome', 'Nome', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::text('nome', Input::old('nome'), array('class' => 'form-control')) }}
                    {{$errors->first('nome')}}
                </div>
            </div>
            <p>
                {{ Form::submit("Adicionar Disciplina", array('class' => 'btn btn-success')) }}
            </p>
        {{ Form::close() }}
    </div>
  </div>

@stop