@section('content')
<!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

  <div id="login-page">
  	<div class="container">
      {{ Form::open(array('url' => 'login', 'class'=>"form-login")) }}
        <h2 class="form-login-heading">acesso o sistema</h2>
        <div class="login-wrap">
            {{ $errors->first('email') }} 
            {{ Form::text('email', Input::old('email'), ['class'=>"form-control"]) }}
            <br>
            {{ $errors->first('password') }} 
            {{ Form::password('password', ['class'=>"form-control"]) }}
            <br>
            {{ Form::button('Entrar', ['class'=>'btn btn-theme btn-block', 'type'=>'submit']) }}
            <hr>
        </div>
      {{ Form::close() }}
  	</div>
  </div>
	  
@stop