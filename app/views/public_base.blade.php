<!doctype html>
<html lang="en">
    <head>
        @section('head')
            <link href='http://fonts.googleapis.com/css?family=Lato:400' rel='stylesheet' type='text/css'>
            <!-- Bootstrap core CSS -->
            {{ HTML::style('assets/css/bootstrap.css') }}
            <!-- Custom styles for this template -->
            {{ HTML::style('assets/css/style.css') }}
            {{ HTML::style('assets/css/style-responsive.css') }}
        @show
    </head>
    <body>
        <div id="wrap" class="public">
            @if ( Session::has('message') )
                <div class="alert alert-info alert-dismissable" style="margin: 20px 100px">
				  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				  {{ Session::get('message') }}
				</div>
            @endif
            @if ( Session::has('errors') )
                <?php foreach( $errors as $e ): ?>
                    <div class="alert alert-warning alert-dismissable" style="margin: 20px 100px">
					  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					  {{ $e }}
					</div>
                <?php endforeach; ?>
            @endif
            @yield('content')
        </div>
        
        {{ HTML::script('assets/js/jquery.js') }}
        {{ HTML::script('assets/js/jquery-1.8.3.min.js') }}
        {{ HTML::script('assets/js/bootstrap.min.js') }}
        <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"/>
        {{ HTML::script('assets/js/jquery.scrollTo.min.js') }}
        {{ HTML::script('assets/js/jquery.nicescroll.js" type="text/javascript') }}
        {{ HTML::script('assets/js/jquery.sparkline.js') }}


        <!--common script for all pages-->
        {{ HTML::script('assets/js/common-scripts.js') }}
    </body>
</html>
