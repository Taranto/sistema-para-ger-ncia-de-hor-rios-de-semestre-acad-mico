@section('sidebar')
<!-- sidebar menu start-->
<ul class="sidebar-menu" id="nav-accordion">

    <p class="centered"><a href="profile.html"><img src="assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
    <h5 class="centered">{{ $data['user'] }}</h5>

    <?php foreach( $data['menu'] as $b ): ?>
        @if (sizeof($b->submenu) > 0)
        <li class="sub-menu dcjq-parent-li">
        @else
        <li class="mt">
        @endif
            <a class="@if ($b->active) active @endif @if (sizeof($b->submenu) > 0) dcjq-parent @endif" href="{{ $b->url }}">
                <i class="fa {{ $b->icon }}"></i>
                <span>{{ $b->name }}</span>
                @if (sizeof($b->submenu) > 0) <span class="dcjq-icon"></span> @endif
            </a>
            @if (sizeof($b->submenu) > 0)
                <ul class="sub" style="display: block;">
                <?php foreach( $b->submenu as $sm ): ?>
                    <li @if ($sm->active) class="active"e @endif><a href="{{ $sm->url }}">{{ $sm->name }}</a></li>
                <?php endforeach; ?>
                </ul>
            @endif
        </li>
    <?php endforeach; ?>
</ul>
<!-- sidebar menu end-->
@stop