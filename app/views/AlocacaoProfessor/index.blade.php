@section('content')

<div class="row mt">
    <div class="col-md-12">
        <div class="content-panel">
            <h4><i class="fa fa-angle-right"></i> Professores Alocados </h4><hr><table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                  <th><i class="fa fa-bullhorn"></i> Nome</th>
                  <th class="hidden-phone"><i class="fa fa-question-circle"></i> Matéria </th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach( $alocacaoP as $a ): ?>
                    <tr>
                        <td><a href="#">{{ Professor::find($a->professor_id)->nome }}</a></td>
                        <td class="hidden-phone">{{ Disciplina::find($a->disciplina_id)->nome }}</td>
                        <td>
                            <a href="{{ URL::to('/alocacaop/remover/'.$a->id) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            
            <p>
                <a href="http://omoraes.com.br/s/public/alocacaop/novo" class="btn btn-success"> Alocar outro Professor </a>
            </p>
        </div><!-- /content-panel -->
    </div><!-- /col-md-12 -->
</div>

@stop