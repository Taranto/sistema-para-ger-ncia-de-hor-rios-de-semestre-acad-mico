@section('content')

<div class="col-lg-12">
    <div class="form-panel">
        <h4 class="mb"><i class="fa fa-angle-right"></i> Atribuir Professor </h4>
          
        {{ Form::open(array('route' => array('alocacaop.save', $matriz->curso_id), 'class' => 'form-horizontal style-form')) }}
            <div class="form-group">
                {{ Form::label('disciplina_ids[]', 'Disciplina', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::select('disciplina_ids[]', $matriz->disciplinas, null, array('multiple'=>'multiple', 'class'=>'form-control')) }}
                    {{$errors->first('disciplina_ids[]')}}
                </div>
            </div>
            
            <div class="form-group">
                {{ Form::label('professor', 'Professor', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::select('professor', $matriz->professores , null, array('class'=>'form-control')) }}
                    {{$errors->first('professor')}}
                </div>
            </div>
            <p>
                {{ Form::submit("Adicionar", array('class' => 'btn btn-success')) }}
            </p>
        {{ Form::close() }}
    </div>
  </div>

@stop