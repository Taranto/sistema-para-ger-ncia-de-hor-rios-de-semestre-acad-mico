@section('content')

<div class="col-lg-12">
    <div class="form-panel">
        <h4 class="mb"><i class="fa fa-angle-right"></i> Editando o Curso {{$curso->curso->nome}}</h4>
          
        {{ Form::open(array('route' => array('curso.update', $curso->curso->id), 'class' => 'form-horizontal style-form')) }}
            <div class="form-group">
                {{ Form::label('professor_id', 'Professor', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::select('professor_id', $curso->professores, $curso->curso->professor_id, array('class'=>'form-control')) }}
                    {{$errors->first('professor_id')}}
                </div>
            </div>
            
            <div class="form-group">
                {{ Form::label('nome', 'Nome', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::text('nome', $curso->curso->nome, array('class' => 'form-control')) }}
                    {{$errors->first('nome')}}
                </div>
            </div>
            <p>
                {{ Form::submit("Modificar Curso", array('class' => 'btn btn-success')) }}
            </p>
        {{ Form::close() }}
    </div>
  </div>

@stop