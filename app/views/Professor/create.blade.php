@section('content')

<div class="col-lg-12">
    <div class="form-panel">
        <h4 class="mb"><i class="fa fa-angle-right"></i> Novo Professor </h4>
          
        {{ Form::open(array('route' => 'professor.save', 'class' => 'form-horizontal style-form')) }}
            <div class="form-group">
                {{ Form::label('tipo', 'Tipo', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::select('tipo', $prof->tipos, null, array('class'=>'form-control')) }}
                    {{$errors->first('tipo')}}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('nome', 'Nome', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::text('nome', Input::old('nome'), array('class' => 'form-control')) }}
                    {{$errors->first('nome')}}
                </div>
            </div>
             <div class="form-group">
                {{ Form::label('email', 'Email', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::text('email', Input::old('email'), array('class' => 'form-control')) }}
                    {{$errors->first('email')}}
                </div>
            </div>
            <p>
                {{ Form::submit("Criar Professor", array('class' => 'btn btn-success')) }}
            </p>
        {{ Form::close() }}
    </div>
  </div>

@stop