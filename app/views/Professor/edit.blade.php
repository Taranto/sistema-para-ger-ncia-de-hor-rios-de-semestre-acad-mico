@section('content')

<div class="col-lg-12">
    <div class="form-panel">
        <h4 class="mb"><i class="fa fa-angle-right"></i> Editando o Professor {{$professor->professor->nome}}</h4>
          
        {{ Form::open(array('route' => array('professor.update', $professor->professor->id), 'class' => 'form-horizontal style-form')) }}
            <div class="form-group">
                {{ Form::label('tipo', 'Tipo', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::select('tipo', $professor->tipos, $professor->professor->tipo, array('class'=>'form-control')) }}
                    {{$errors->first('tipo')}}
                </div>
            </div>
            
            <div class="form-group">
                {{ Form::label('nome', 'Nome', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::text('nome', $professor->professor->nome, array('class' => 'form-control')) }}
                    {{$errors->first('nome')}}
                </div>
            </div>
            <p>
                {{ Form::submit("Modificar Professor", array('class' => 'btn btn-success')) }}
            </p>
        {{ Form::close() }}
    </div>
  </div>

@stop