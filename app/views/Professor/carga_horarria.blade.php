@section('content')
<h3><i class="fa fa-angle-right"></i> Carga Horária</h3>
 
<div class="row mt">
  <aside class="col-lg-9 mt">
      <section class="panel">
          <div class="panel-body">
                <!--<div id="calendar" class="has-toolbar"></div>  -->
                <table id="calendar_fulero" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Carga Horária</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach( $cargas as $a ): ?>
                            <tr>
                                <td>{{ $a['nome'] }}</td>
                                <td>{{ $a['cargaHoraria'] }}</td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
      </section>
  </aside>
</div>
@stop