@section('content')

<div class="row mt">
    <div class="col-md-12">
        <div class="content-panel">
            <h4><i class="fa fa-angle-right"></i> Areas </h4><hr><table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                  <th><i class="fa fa-bullhorn"></i> Nome</th>
                  <th class="hidden-phone"><i class="fa fa-question-circle"></i> Supervisor </th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach( $areas as $d ): ?>
                    <tr>
                        <td><a href="#">{{ $d->nome }}</a></td>
                        <td class="hidden-phone">{{ Area::find($d->id)->professor->nome }}</td>
                        <td>
                            <a href="{{ URL::to('/area/'.$d->id.'/edit') }}" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                        </td>
                        <td>
                            <a href="{{ URL::to('/area/remover/'.$d->id) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            {{ $areas->links() }}
            <p>
                <a href="http://omoraes.com.br/s/public/area/novo" class="btn btn-success"> Criar Area </a>
            </p>
        </div><!-- /content-panel -->
    </div><!-- /col-md-12 -->
</div>

@stop