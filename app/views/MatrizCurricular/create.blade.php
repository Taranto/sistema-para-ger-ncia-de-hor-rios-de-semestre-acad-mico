@section('content')

<div class="col-lg-12">
    <div class="form-panel">
        <h4 class="mb"><i class="fa fa-angle-right"></i> Criar Matriz Curricular </h4>
          
        {{ Form::open(array('route' => array('matriz.save', $matriz->curso_id), 'class' => 'form-horizontal style-form')) }}
            <div class="form-group">
                {{ Form::label('disciplina_ids[]', 'Disciplina', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::select('disciplina_ids[]', $matriz->disciplinas, null, array('multiple'=>'multiple', 'class'=>'form-control')) }}
                    {{$errors->first('disciplina_ids[]')}}
                </div>
            </div>
            
            <div class="form-group">
                {{ Form::label('semestre', 'Semestre', array('class'=> 'col-sm-2 col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::select('semestre', array(1=>1, 2=>2, 3=>3, 4=>4, 5=>5, 6=>6, 7=>7, 8=>8,91=>9, 10=>10) , null, array('class'=>'form-control')) }}
                    {{$errors->first('semestre')}}
                </div>
            </div>
            <p>
                {{ Form::submit("Adicionar Matriz", array('class' => 'btn btn-success')) }}
            </p>
        {{ Form::close() }}
    </div>
  </div>

@stop