@section('content')

<div class="row mt">
    <div class="col-md-12">
        <div class="content-panel">
            <h4><i class="fa fa-angle-right"></i> Matriz Curricular </h4><hr>
            <?php foreach( $matriz->disciplinas as $semestre => $dgrande ): ?>
                <h2>{{ $semestre }}º Semestre</h2>
                <ul>
                <?php foreach( $dgrande as $d ): ?>
                    <li>{{ $d->nome }}  <a href="{{ URL::to('/matriz/remover/'.$matriz->curso_id.'/'.$d->id) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a> </li>
                <?php endforeach; ?>
                </ul>
            <?php endforeach; ?>
            <p>
                <a href="http://omoraes.com.br/s/public/matriz/novo" class="btn btn-success"> Adicionar nova matriz </a>
            </p>
        </div><!-- /content-panel -->
    </div><!-- /col-md-12 -->
</div>

@stop