
@section('header')
    <div id="logo">
        <h1><img src="<?php echo URL::asset('logo.png'); ?>" /> <?php echo $header['title'] ?> </h1>
    </div>

    @if( isset($header['menu']) )
        <nav id="topmenu">
            <ul>
                <?php foreach($header['menu'] as $btn): ?>
                    <li><a class="<?php if($btn->current) echo 'current' ?>" href="<?php echo $btn->link ?>"><?php echo $btn->label ?></a></li>
                <?php endforeach; ?>
            </ul>
        </nav>

        <div class="logout pull-right">
            {{ HTML::link(URL::to('/logout'), trans('user.logout')) }}
        </div>
    @endif

@stop