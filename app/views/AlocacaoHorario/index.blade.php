@section('content')
    <link href="assets/js/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
<h3><i class="fa fa-angle-right"></i> Calendar</h3>
 
<div class="row mt">
                  <aside class="col-lg-3 mt">
                        <h4><i class="fa fa-angle-right"></i> Disciplina/Professor </h4>
                        <div id="external-events" data-init="true" ondrop="drophome(event)" ondragover="allowDrop(event)">
                        <?php $c=0; foreach( $data->naoalocado as $a ): ?>
                            <div id="naoal{{ $c++ }}" draggable="true" ondragstart="drag(event)" data-alloc="false" class="external-event label label-info" data-id="{{ $a->id }}">
                                {{ Professor::find($a->professor_id)->nome }} | {{ Disciplina::find($a->disciplina_id)->nome }}
                            </div>
                        <?php endforeach; ?>
                        </div>
                  </aside>
                  </aside>
                  <aside class="col-lg-9 mt">
                      <section class="panel">
                          <div class="panel-body">
                                <!--<div id="calendar" class="has-toolbar"></div>  -->
                                <table id="calendar_fulero" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Segunda</th>
                                            <th>Terça</th>
                                            <th>Quarta</th>
                                            <th>Quinta</th>
                                            <th>Sexta</th>
                                            <th>Sábado</th>
                                            <th>Domingo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="diurno_um" data-turno="diurno um">
                                            <td class="slot labelc">Diurno Um</td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="seg"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="ter"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="qua"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="qui"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="sex"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="sab"></td>
                                            <td class="slot dom"></td>
                                        </tr>
                                        <tr class="diurno_dois" data-turno="diurno dois">
                                            <td class="slot labelc">Diurno Dois</td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="seg"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="ter"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="qua"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="qui"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="sex"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="sab"></td>
                                            <td class="slot dom"></td>
                                        </tr>
                                        <tr class="noturno" data-turno="noturno">
                                            <td class="slot labelc">Noturno</td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="seg"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="ter"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="qua"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="qui"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="sex"></td>
                                            <td ondrop="drop(event)" ondragover="allowDrop(event)" class="slot" data-data="sab"></td>
                                            <td class="slot dom"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                      </section>
                  </aside>
              </div>
@stop

@section('footer')
    {{ HTML::script('assets/js/jquery-1.8.3.min.js') }}
    <script>
      //custom select box

      /*$(function(){
          $("select.styled").customSelect();
      });*/
      
        function remove(el) {
            console.log(el.dataset.alloc);
            if(el.dataset.alloc == 'true') {
                // json to remove stuff
                $.ajax({
                    url: "{{ URL::to('/alocacaoh/remover') }}/" + el.dataset.alhid,
                    type: 'DELETE',
                    success: function(result) {
                        // Do something with the result
                        el.parentNode.removeChild(el);
                    }
                });
            }else{
                el.parentNode.removeChild(el);
            }
        }
        
        function additionate(el, targeto) {
            if(targeto.dataset.init !== 'true') {
                dataAl = {
                    'dia': targeto.dataset.data,
                    'turno': targeto.parentNode.dataset.turno,
                    'alocacao_professor_id': el.dataset.id
                }
                // json to add stuff
                $.ajax({
                    type: "POST",
                    //the url where you want to sent the userName and password to
                    url: "{{ URL::to('/alocacaoh/novo') }}",
                    dataType: 'json',
                    async: false,
                    //json object to sent to the authentication url
                    data: JSON.stringify(dataAl),
                    success: function (data) {
                        console.log(data);
                        remove(el);
                        el.dataset.alloc = "true";
                        el.dataset.alhid = data.id;
                        targeto.appendChild(el.cloneNode(true));
                    },
                    error: function(data){
                        console.log(data);
                        alert('error');
                    }
                })
            } else {
                el.dataset.alloc = "false";
                targeto.appendChild(el.cloneNode(true));
                remove(el);
            }
        }
      
        function allowDrop(ev) {
            ev.preventDefault();
        }
        
        function drag(ev) {
            ev.dataTransfer.setData("Text", ev.target.id);
        }
        
        function drop(ev) {
            ev.preventDefault();
            var data=ev.dataTransfer.getData("Text");
            
            el = document.getElementById(data);
            targeto = ev.target;
            
            if( targeto.dataset.alloc === "true" || targeto.dataset.alloc === "false" )
                targeto = targeto.parentNode;
            additionate(el, targeto);
        }
        
        function drophome(ev) {
            ev.preventDefault();
            var data=ev.dataTransfer.getData("Text");
            
            el = document.getElementById(data);
            targeto = ev.target;
            
            if( targeto.dataset.alloc === "true" || targeto.dataset.alloc === "false" )
                targeto = targeto.parentNode; 
                
            targeto.appendChild(el.cloneNode(true));
            remove(el);
        }
        
        var alocados = [
        <?php foreach( $data->alocado as $a ): ?>
            {   
                'turno': '{{ $a->alocacaoHorario->turno }}',
                'dia': '{{ $a->alocacaoHorario->dia }}',
                'item': '<div id="al{{  $a->alocacaoHorario->id }}" draggable="true" ondragstart="drag(event)" data-alloc="true" class="external-event label label-info" data-alhid="{{ $a->alocacaoHorario->id }}" data-id="{{ $a->id }}">{{ Professor::find($a->professor_id)->nome }} | {{ Disciplina::find($a->disciplina_id)->nome }}</div>'
            },
        <?php endforeach; ?>
        ];
        
        $(document).ready(function(){
            alocados.forEach(function(e){
                turn = e.turno;
                dei = e.dia;
                $("#calendar_fulero").find("[data-turno='" + turn + "'] [data-data='" + dei + "']").append(e.item);
            });
        });

  </script>
@stop