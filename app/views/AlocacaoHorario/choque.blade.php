@section('content')
<h3><i class="fa fa-angle-right"></i> Choque Horario</h3>
 
<div class="row mt">
  <aside class="col-lg-9 mt">
      <section class="panel">
          <div class="panel-body">
                <!--<div id="calendar" class="has-toolbar"></div>  -->
                <table id="calendar_fulero" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Professores</th>
                            <th>Motivo</th>
                            <th>Curso</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach( $lista as $a ): ?>
                            <tr>
                                <td>{{ Professor::find($a['aloc1']->alocacaoProfessor->professor_id)->nome }} -
                                {{Professor::find($a['aloc2']->alocacaoProfessor->professor_id)->nome }}</td> 
                                <td>{{ $a['motivo'] }}</td>
                                <td>{{ $a['aloc1']->alocacaoProfessor->curso->nome }} -
                                {{ $a['aloc2']->alocacaoProfessor->curso->nome }}</td>
                            </tr>
                        <?php endforeach; ?> 
                    </tbody>
                </table>
            </div>
      </section>
  </aside>
</div>
@stop