@section('content')


    <div class="row">
      <div class="col-lg-9 main-chart">

        <div class="row mtbox">
            <div class="col-md-2 col-sm-2 col-md-offset-1 box0">
                <div class="box1">
                    <span class="li_heart"></span>
                    <h3>933</h3>
                </div>
                    <p>933 People liked your page the last 24hs. Whoohoo!</p>
            </div>
            <div class="col-md-2 col-sm-2 box0">
                <div class="box1">
                    <span class="li_cloud"></span>
                    <h3>+48</h3>
                </div>
                    <p>48 New files were added in your cloud storage.</p>
            </div>
            <div class="col-md-2 col-sm-2 box0">
                <div class="box1">
                    <span class="li_stack"></span>
                    <h3>23</h3>
                </div>
                    <p>You have 23 unread messages in your inbox.</p>
            </div>
            <div class="col-md-2 col-sm-2 box0">
                <div class="box1">
                    <span class="li_news"></span>
                    <h3>+10</h3>
                </div>
                    <p>More than 10 news were added in your reader.</p>
            </div>
            <div class="col-md-2 col-sm-2 box0">
                <div class="box1">
                    <span class="li_data"></span>
                    <h3>OK!</h3>
                </div>
                    <p>Your server is working perfectly. Relax &amp; enjoy.</p>
            </div>

        </div><!-- /row mt -->

      </div><!-- /col-lg-9 END SECTION MIDDLE -->


    <!-- **********************************************************************************************************************************************************
    RIGHT SIDEBAR CONTENT
    *********************************************************************************************************************************************************** -->

      <div class="col-lg-3 ds">
        <!--COMPLETED ACTIONS DONUTS CHART-->
            <h3>NOTIFICATIONS</h3>

          <!-- First Action -->
          <div class="desc">
            <div class="thumb">
                <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
            </div>
            <div class="details">
                <p><muted>2 Minutes Ago</muted><br>
                   <a href="#">James Brown</a> subscribed to your newsletter.<br>
                </p>
            </div>
          </div>
          <!-- Second Action -->
          <div class="desc">
            <div class="thumb">
                <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
            </div>
            <div class="details">
                <p><muted>3 Hours Ago</muted><br>
                   <a href="#">Diana Kennedy</a> purchased a year subscription.<br>
                </p>
            </div>
          </div>
          <!-- Third Action -->
          <div class="desc">
            <div class="thumb">
                <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
            </div>
            <div class="details">
                <p><muted>7 Hours Ago</muted><br>
                   <a href="#">Brandon Page</a> purchased a year subscription.<br>
                </p>
            </div>
          </div>
          <!-- Fourth Action -->
          <div class="desc">
            <div class="thumb">
                <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
            </div>
            <div class="details">
                <p><muted>11 Hours Ago</muted><br>
                   <a href="#">Mark Twain</a> commented your post.<br>
                </p>
            </div>
          </div>
          <!-- Fifth Action -->
          <div class="desc">
            <div class="thumb">
                <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
            </div>
            <div class="details">
                <p><muted>18 Hours Ago</muted><br>
                   <a href="#">Daniel Pratt</a> purchased a wallet in your store.<br>
                </p>
            </div>
          </div>

            <!-- CALENDAR-->
            <div class="mb" id="calendar">
                <div class="panel green-panel no-margin">
                    <div class="panel-body">
                        <div style="cursor: pointer; margin-left: 33%; margin-top: -50px; width: 175px; display: none;" class="popover top" id="date-popover" data-original-title="" title="">
                            <div class="arrow"></div>
                            <h3 style="disadding: none;" class="popover-title"></h3>
                            <div class="popover-content" id="date-popover-content"></div>
                        </div>
                        <div id="zabuto_calendar_jlq"><div id="zabuto_calendar_jlq" class="zabuto_calendar"><table class="table"><tbody><tr class="calendar-month-header"><th><div class="calendar-month-navigation" id="zabuto_calendar_jlq_nav-prev"><span><span class="fa fa-chevron-left text-transparent"></span></span></div></th><th colspan="5"><span>September 2014</span></th><th><div class="calendar-month-navigation" id="zabuto_calendar_jlq_nav-next"><span><span class="fa fa-chevron-right text-transparent"></span></span></div></th></tr><tr class="calendar-dow-header"><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th><th>Sun</th></tr><tr class="calendar-dow"><td id="zabuto_calendar_jlq_2014-09-01" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-01_day">1</div></td><td id="zabuto_calendar_jlq_2014-09-02" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-02_day">2</div></td><td id="zabuto_calendar_jlq_2014-09-03" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-03_day">3</div></td><td id="zabuto_calendar_jlq_2014-09-04" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-04_day">4</div></td><td id="zabuto_calendar_jlq_2014-09-05" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-05_day">5</div></td><td id="zabuto_calendar_jlq_2014-09-06" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-06_day">6</div></td><td id="zabuto_calendar_jlq_2014-09-07" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-07_day">7</div></td></tr><tr class="calendar-dow"><td id="zabuto_calendar_jlq_2014-09-08" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-08_day">8</div></td><td id="zabuto_calendar_jlq_2014-09-09" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-09_day">9</div></td><td id="zabuto_calendar_jlq_2014-09-10" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-10_day">10</div></td><td id="zabuto_calendar_jlq_2014-09-11" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-11_day">11</div></td><td id="zabuto_calendar_jlq_2014-09-12" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-12_day">12</div></td><td id="zabuto_calendar_jlq_2014-09-13" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-13_day">13</div></td><td id="zabuto_calendar_jlq_2014-09-14" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-14_day">14</div></td></tr><tr class="calendar-dow"><td id="zabuto_calendar_jlq_2014-09-15" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-15_day">15</div></td><td id="zabuto_calendar_jlq_2014-09-16" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-16_day">16</div></td><td id="zabuto_calendar_jlq_2014-09-17" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-17_day">17</div></td><td id="zabuto_calendar_jlq_2014-09-18" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-18_day">18</div></td><td id="zabuto_calendar_jlq_2014-09-19" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-19_day">19</div></td><td id="zabuto_calendar_jlq_2014-09-20" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-20_day">20</div></td><td id="zabuto_calendar_jlq_2014-09-21" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-21_day">21</div></td></tr><tr class="calendar-dow"><td id="zabuto_calendar_jlq_2014-09-22" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-22_day">22</div></td><td id="zabuto_calendar_jlq_2014-09-23" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-23_day">23</div></td><td id="zabuto_calendar_jlq_2014-09-24" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-24_day">24</div></td><td id="zabuto_calendar_jlq_2014-09-25" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-25_day">25</div></td><td id="zabuto_calendar_jlq_2014-09-26" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-26_day">26</div></td><td id="zabuto_calendar_jlq_2014-09-27" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-27_day">27</div></td><td id="zabuto_calendar_jlq_2014-09-28" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-28_day">28</div></td></tr><tr class="calendar-dow"><td id="zabuto_calendar_jlq_2014-09-29" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-29_day">29</div></td><td id="zabuto_calendar_jlq_2014-09-30" class="dow-clickable"><div class="day" id="zabuto_calendar_jlq_2014-09-30_day">30</div></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table><div id="zabuto_calendar_jlq_legend" class="legend"><span class="legend-text"><span class="badge badge-event">00</span> Special event</span><span class="legend-block"><ul class="legend"><li class="event"></li><span>Regular event</span></ul></span></div></div></div>
                    </div>
                </div>
            </div><!-- / calendar -->

      </div><!-- /col-lg-3 -->
  </div>

@stop
